let deserializedState;

export const loadState = () => {
  try {
    if (!deserializedState) {
      const serializedState = localStorage.getItem('state');
      if (serializedState !== null) {
        deserializedState = JSON.parse(serializedState);
      }
    }
    return deserializedState;
  } catch (err) {
    console.error(err);
  }

  return null;
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (err) {
    console.error(err);
  }
};
