const createQuery = (options) => {
  let query = '?';

  if (options.search !== '') {
    query += 'author_like=';
    query += options.search;
    query += '&';
  }

  if (options.sortBy.column !== '') {
    query += '_sort=';
    query += options.sortBy.column;
    if (options.sortBy.ascending) {
      query += '&_order=ASC';
    } else {
      query += '&_order=DESC';
    }
  }

  return query;
};

export {
  createQuery,
};

export default {
  createQuery,
};
