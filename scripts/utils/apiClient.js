import Promise from 'bluebird';
import api from 'superagent';
import _ from 'lodash';
import { showMessage } from '../redux/actions/notificationActions';
import config from '../config';

const baseUrl = config.getUrlAddresses().backendBaseUrl;

const headers = (req) => {
  req
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('Cache-Control', 'no-cache')
    .set('dataType', 'application/json');
};

const parseError = (err, res) => {
  const error = err || res.error;
  if (!error) {
    return null;
  }
  return res ? res.body || error : error;
};

function handleResponse(resolve, reject) {
  return (err, res) => {
    if (err && err.status === 400) {
      let errorMsgs = Object.keys(err.response.body).map(key => err.response.body[key]).join(', ');
      if (errorMsgs.length === 0) {
        errorMsgs = 'Validation error occured';
      }
      showMessage(errorMsgs);
    }

    const error = parseError(err, res);
    if (error) {
      reject(error);
    } else {
      resolve(res.body);
    }
  };
}

export function get(route, query) {
  return new Promise((resolve, reject) => {
    api.get(baseUrl + route)
      .use(headers)
      .query(query)
      .end(handleResponse(resolve, reject));
  });
}

export function post(route, data) {
  return new Promise((resolve, reject) => {
    api.post(baseUrl + route)
      .use(headers)
      .send(data)
      .end(handleResponse(resolve, reject));
  });
}

export function put(route, data) {
  return new Promise((resolve, reject) => {
    api.put(baseUrl + route)
      .use(headers)
      .send(data)
      .end(handleResponse(resolve, reject));
  });
}

export function del(route) {
  return new Promise((resolve, reject) => {
    api.del(baseUrl + route)
      .use(headers)
      .end(handleResponse(resolve, reject));
  });
}

export function upload(route, files) {
  return new Promise((resolve, reject) => {
    const req = api.post(baseUrl + route);

    if (_.isArray(files)) {
      files.forEach((file) => {
        req.attach(file.name, file);
      });
    }
    req.end(handleResponse(resolve, reject));
  });
}
