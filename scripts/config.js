function getUrlAddresses() {
  const urlAddresses = {
    backendBaseUrl: 'http://localhost:3000',
  };

  return urlAddresses;
}

export default { getUrlAddresses };
