import _ from 'lodash';
import { actionTypes } from '../actions/postHistoryActions';
import { saveState } from '../../utils/localStorage';

const initialState = {
  data: [],
  options: {
    sortBy: {
      column: '',
      ascending: true,
    },
    search: '',
    page: 1,
    itemsPerPage: 10,
  },
  isDataLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.getDataRequest: {
      return _.assign({}, state, { isDataLoading: true });
    }

    case actionTypes.getDataSuccess: {
      return _.assign({}, state, { data: action.data, isDataLoading: false });
    }

    case actionTypes.getDataFailure: {
      return _.assign({}, state, { error: action.error, isDataLoading: false });
    }

    case actionTypes.setOptions: {
      saveState(action.options);
      return _.assign({}, state, { options: action.options });
    }

    default:
      return state;
  }
};
