import _ from 'lodash';
import { actionTypes } from '../actions/notificationActions';


const initialState = {
  snackbarOpen: false,
  message: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.showSnackbar:
      return _.assign({}, state, { snackbarOpen: action.data });
    case actionTypes.showMessage:
      return _.assign({}, state, { message: action.data, snackbarOpen: true });
    default:
      return state;
  }
};
