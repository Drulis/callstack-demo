import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import securityReducer from './securityReducer';
import notificationReducer from './notificationReducer';
import postHistoryReducer from './postHistoryReducer';

export default combineReducers({
  routing: routerReducer,
  postHistory: postHistoryReducer,
  security: securityReducer,
  notification: notificationReducer,
});
