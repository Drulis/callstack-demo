import _ from 'lodash';
import { actionTypes } from '../actions/securityActions';

const initialState = {
  loggedIn: '',
  isUserLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.getUserRequest: {
      return _.assign({}, state, { isUserLoading: true });
    }
    case actionTypes.getUserSuccess: {
      return _.assign({}, state, { loggedIn: action.name, isUserLoading: false });
    }
    case actionTypes.getUserFailure: {
      return _.assign({}, state, { error: action.error, isUserLoading: false });
    }
    default:
      return state;
  }
};
