import _ from 'lodash';
import store from '../store';
import { get, post } from '../../utils/apiClient';
import { createQuery } from '../../utils/queryHelper';
import { loadState } from '../../utils/localStorage';

const apiRoutes = {
  postHistory: '/postHistory',
};

export const actionTypes = {
  getDataRequest: 'GET_POST_HISTORY_DATA_REQUEST',
  getDataSuccess: 'GET_POST_HISTORY_DATA_SUCCESS',
  getDataFailure: 'GET_POST_HISTORY_DATA_FAILED',
  addPostRequest: 'ADD_NEW_POST_REQUEST',
  addPostSuccess: 'ADD_NEW_POST_SUCCESS',
  addPostFailure: 'ADD_NEW_POST_FAILURE',
  setOptions: 'SET_OPTIONS',
};

const getDataRequestAction = () => ({
  type: actionTypes.getDataRequest,
});

const getDataSuccessAction = data => ({
  type: actionTypes.getDataSuccess,
  data,
});

const getDataFailureAction = error => ({
  type: actionTypes.getDataFailure,
  error,
});

const addPostRequestAction = () => ({
  type: actionTypes.addPostRequest,
});

const addPostSuccessAction = () => ({
  type: actionTypes.addPostFailure,
});

const addPostFailureAction = error => ({
  type: actionTypes.addPostRequest,
  error,
});

export const addPost = (newPost) => {
  store.dispatch({
    type: actionTypes.addPost,
    isSaving: true,
  });
  return post(apiRoutes.postHistory, newPost)
    .then(() => {
      store.dispatch({
        type: actionTypes.getData,
        isSaving: false,
      });
      return null;
    })
    .catch((error) => {
      store.dispatch({
        type: actionTypes.getData,
        error,
      });
    });
};

export const addPostThunk = (newPost) => {
  return (dispatch) => {
    dispatch(addPostRequestAction());
    return post(apiRoutes.postHistory, newPost)
      .then(() => dispatch(addPostSuccessAction()))
      .catch(error => dispatch(addPostFailureAction(error)));
  };
};

export const getDataThunk = (options) => {
  return (dispatch) => {
    dispatch(getDataRequestAction());
    get(apiRoutes.postHistory + createQuery(options))
      .then(data => dispatch(getDataSuccessAction(data)))
      .catch(error => dispatch(getDataFailureAction(error)));
  };
};

export const setOptions = newOptions => ({
  type: actionTypes.setOptions,
  options: newOptions,
});

export const loadOptionsAndDataThunk = (currentOptions) => {
  return (dispatch) => {
    const savedOptions = loadState();
    if (!_.isEmpty(savedOptions)) {
      dispatch(setOptions(savedOptions));
      dispatch(getDataThunk(savedOptions));
    } else {
      dispatch(getDataThunk(currentOptions));
    }
  };
};
