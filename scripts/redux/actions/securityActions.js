import store from '../store';
import { get } from '../../utils/apiClient';

const apiRoutes = {
  getLoggedIn: '/loggedIn/',
};

const actionTypes = {
  getLoggedInUser: 'GET_LOGGED_IN_USER',
  getUserRequest: 'GET_LOGGED_IN_USER_REQUEST',
  getUserSuccess: 'GET_LOGGED_IN_USER_SUCCESS',
  getUserFailure: 'GET_LOGGED_IN_USER_FAILURE',
};

const getUserRequest = () => ({
  type: actionTypes.getUserRequest,
});

const getUserSuccess = data => ({
  type: actionTypes.getUserRequest,
  name: data.name,
});

const getUserFailure = error => ({
  type: actionTypes.getUserRequest,
  error,
});

export const getLoggedInUserThunk = () => {
  return (dispatch) => {
    dispatch(getUserRequest());
    get(apiRoutes.getLoggedIn)
      .then(data => dispatch(getUserSuccess(data)))
      .catch(error => dispatch(getUserFailure(error)));
  };
};

const getLoggedInUser = () => {
  store.dispatch({
    type: actionTypes.getLoggedInUser,
  });
  get(apiRoutes.getLoggedIn)
    .then((data) => {
      store.dispatch({
        type: actionTypes.getLoggedInUser,
        name: data.name,
      });
    })
    .catch((error) => {
      store.dispatch({
        type: actionTypes.getLoggedInUser,
        error,
      });
    });
};

export {
  actionTypes,
  getLoggedInUser,
};

export default {
  actionTypes,
  getLoggedInUser,
};
