import store from '../store';

const actionTypes = {
  showSnackbar: 'SHOW_SNACKBAR',
  showMessage: 'SHOW_MESSAGE',
};

const showMessage = (message) => {
  store.dispatch({
    type: actionTypes.showMessage,
    data: message,
  });
};

const showSnackbar = (value) => {
  store.dispatch({
    type: actionTypes.showSnackbar,
    data: value,
  });
};

export {
  actionTypes,
  showMessage,
  showSnackbar,
};

export default {
  actionTypes,
  showMessage,
  showSnackbar,
};

