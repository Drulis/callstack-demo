import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Uuid from 'uuid';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Store
import store from './redux/store';
// Components
import Layout from './components/layout/layout';
import PostHistory from './views/postHistory';

import '../assets/scss/app.scss';
// material-ui
injectTapEventPlugin();

const history = syncHistoryWithStore(hashHistory, store);

class App extends React.Component {

  componentWillMount() {
    const routes = [];
    routes.push({
      path: '/',
      component: Layout,
      indexRoute: { component: PostHistory },
      childRoutes: [],
    });

    this.routes = routes;
  }

  render() {
    return (
      <Router key={Uuid()} history={history} routes={this.routes} />
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('main'));

if (module.hot) {
  module.hot.accept();
}
