import React, { PropTypes, Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Paper, TextField } from 'material-ui';
import DataTable from '../components/table/dataTable';
import DataTablePagination from '../components/table//dataTablePagination';
import AddPostForm from '../components/table/AddPostForm';
import { setOptions, loadOptionsAndDataThunk, getDataThunk, addPostThunk } from '../redux/actions/postHistoryActions';
import { getLoggedInUserThunk } from '../redux/actions/securityActions';

class PostHistory extends Component {
  static get propTypes() {
    return {
      data: PropTypes.array.isRequired,
      options: PropTypes.object.isRequired,
      loggedIn: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    this.props.actions.loadOptionsAndDataThunk(this.props.options);
    this.props.actions.getLoggedInUserThunk();
  }

  _onSearchChange = (e) => {
    const newOptions = _.assign({}, this.props.options, { page: 1, search: e.target.value });
    this.props.actions.setOptions(newOptions);
    this.props.actions.getDataThunk(newOptions);
  }

  render() {
    const { page, itemsPerPage } = this.props.options;
    const paginatedData = _.slice(this.props.data, (page - 1) * itemsPerPage, page * itemsPerPage);
    return (
      <Paper className="tableComponent">
        <div>
          Search:
          <TextField
            style={{ marginLeft: 10 }}
            name="searchBox"
            value={this.props.options.search}
            onChange={this._onSearchChange}
          />
        </div>

        <div className="tablePlaceholder">
          <DataTable
            className="clearfix"
            data={paginatedData}
            options={this.props.options}
            loggedIn={this.props.loggedIn}
            setOptions={this.props.actions.setOptions}
            refreshData={this.props.actions.getDataThunk}
          />
          <AddPostForm
            data={this.props.data}
            options={this.props.options}
            refreshData={this.props.actions.getDataThunk}
            addRecord={this.props.actions.addPostThunk}
          />
        </div>

        <DataTablePagination
          setOptions={this.props.actions.setOptions}
          count={this.props.data.length}
          options={this.props.options}
        />
      </Paper>
    );
  }
}

const mapDispatchToProps = dispatch => (
  {
    actions: bindActionCreators(
      {
        setOptions,
        getDataThunk,
        loadOptionsAndDataThunk,
        getLoggedInUserThunk,
        addPostThunk,
      },
      dispatch,
    ),
  }
);

const mapStateToProps = state => (
  {
    data: state.postHistory.data,
    options: state.postHistory.options,
    isDataLoading: state.postHistory.isDataLoading,
    loggedIn: state.security.loggedIn,
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(PostHistory);
