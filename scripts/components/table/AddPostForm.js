import React from 'react';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
  buttonWrapper: {
    float: 'right',
  },
};

export default class AddPostForm extends React.Component {

  _onAdd = () => {
    const newId = _.maxBy(this.props.data, (o) => { return o.id; }).id + 1;
    console.log(newId);
    const newPost = {
      id: newId,
      // note: usually i dislike using refs but here it's a neccesary workaround
      // as material-ui table takes focus from inputs inside of it on re-render
      // by using refs, inputs are self controlled
      author: this.authorField.getValue(),
      title: this.titleField.getValue(),
      views: this.viewsField.getValue(),
      likes: this.likesField.getValue(),
      created: this.createdField.getValue(),
    };
    this.props.addRecord(newPost).then(
      this.props.refreshData(this.props.options),
    );
  }

  render() {
    return (
      <div>
        <Table selectable={false} >
          <TableBody
            displayRowCheckbox={false}
            deselectOnClickaway={false}
          >
            <TableRow>
              <TableRowColumn width={70} />
              <TableRowColumn>
                <TextField
                  name="author"
                  ref={(c) => { this.authorField = c; }}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  name="title"
                  ref={(c) => { this.titleField = c; }}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  name="views"
                  ref={(c) => { this.viewsField = c; }}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  name="likes"
                  ref={(c) => { this.likesField = c; }}
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField
                  name="created"
                  ref={(c) => { this.createdField = c; }}
                />
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
        <div className="buttonWrapper" style={styles.buttonWrapper} >
          <RaisedButton label="Add" primary onClick={this._onAdd} />
        </div>
      </div>
    );
  }
}
