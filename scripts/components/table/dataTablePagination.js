import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { HardwareKeyboardArrowLeft, HardwareKeyboardArrowRight } from 'material-ui/svg-icons';
import { IconButton, SelectField, MenuItem } from 'material-ui';

export default class DataTablePagination extends Component {
  static get propTypes() {
    return {
      options: PropTypes.object.isRequired,
      count: PropTypes.number.isRequired,
    };
  }

  _setItemsPerPage = (event, index, value) => {
    const newOptions = _.assign({}, this.props.options, { page: 1, itemsPerPage: value });
    this.props.setOptions(newOptions);
  }

  _nextPage = () => {
    if (this.props.options.page < Math.ceil(this.props.count / this.props.options.itemsPerPage)) {
      const newOptions = _.assign({}, this.props.options, { page: this.props.options.page + 1 });
      this.props.setOptions(newOptions);
    }
  }

  _previewPage = () => {
    if (this.props.options.page > 1) {
      const newOptions = _.assign({}, this.props.options, { page: this.props.options.page - 1 });
      this.props.setOptions(newOptions);
    }
  }

  _setPage = (value) => {
    const newOptions = _.assign({}, this.props.options, { page: value });
    this.props.setOptions(newOptions);
  }

  render() {
    const paginationNumbers = [];
    for (let i = 1; i <= Math.ceil(this.props.count / this.props.options.itemsPerPage); i += 1) {
      if (i === this.props.options.page) {
        paginationNumbers.push(
          <li
            key={i}
            className="paginationNumber currentNumber"
            onClick={() => this._setPage(i)}
          >
            {i}
          </li>,
        );
      } else {
        paginationNumbers.push(
          <li
            key={i}
            className="paginationNumber"
            onClick={() => this._setPage(i)}
          >
            {i}
          </li>,
        );
      }
    }
    return (
      <div className="tableComponentPagination">
        <div className="qtyResults">
          <SelectField
            className="selectResultsPerPage"
            floatingLabelText="Results per page"
            value={this.props.options.itemsPerPage}
            onChange={this._setItemsPerPage}
          >
            <MenuItem value={5} primaryText="5" />
            <MenuItem value={10} primaryText="10" />
            <MenuItem value={15} primaryText="15" />
          </SelectField>
          <div className="showingPerPage">
            {`Showing from ${Math.min(((this.props.options.page - 1) * this.props.options.itemsPerPage) + 1, this.props.count)}  
              to ${(Math.min(((this.props.options.page) * this.props.options.itemsPerPage), this.props.count))} of ${this.props.count}  results.`}
          </div>
        </div>
        <div className="tablePagination">
          <ul className="componentPagination">
            <li className="paginationArrow arrowLeft">
              <IconButton tooltip="Preview" onClick={this._previewPage}>
                <HardwareKeyboardArrowLeft />
              </IconButton>
            </li>
            {paginationNumbers}
            <li className="paginationArrow arrowRight">
              <IconButton tooltip="Next" onClick={this._nextPage}>
                <HardwareKeyboardArrowRight />
              </IconButton>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
