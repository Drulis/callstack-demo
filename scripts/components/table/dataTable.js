import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import Uuid from 'uuid';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { NavigationArrowDownward, NavigationArrowUpward } from 'material-ui/svg-icons';

export default class DataTable extends Component {
  static get propTypes() {
    return {
      loggedIn: PropTypes.string,
      data: PropTypes.array,
      options: PropTypes.shape({
        sortBy: PropTypes.shape({
          column: PropTypes.string,
          ascending: PropTypes.bool,
        }),
      }),
    };
  }

  _setOrderBy = (field) => {
    let column = '';
    let ascending = false;
    if (this.props.options.sortBy.column === field) {
      if (this.props.options.sortBy.ascending) {
        // set desc
        column = field;
        ascending = false;
      } else {
        // turn off sort
        column = '';
      }
    } else {
      // turn on sort
      column = field;
      ascending = true;
    }
    const newOptions = _.assign({}, this.props.options, { sortBy: { column, ascending } });
    this.props.setOptions(newOptions);
    this.props.refreshData(newOptions);
  }

  _addSortArrow = (column) => {
    if (column === this.props.options.sortBy.column) {
      if (this.props.options.sortBy.ascending) {
        return (
          <NavigationArrowUpward
            className="sortIcon sortIconActive"
            id={column}
          />
        );
      }
      return (
        <NavigationArrowDownward
          className="sortIcon sortIconActive"
          id={column}
        />
      );
    }
    return (
      <NavigationArrowDownward
        className="sortIcon"
        id={column}
      />
    );
  }

  render() {
    return (
      <div className="table">
        <Table
          selectable={false}
        >
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
          >
            <TableRow>
              <TableHeaderColumn key={'id'}>
                {this._addSortArrow('id')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'id')}>Id</span>
              </TableHeaderColumn>
              <TableHeaderColumn key={'author'}>
                {this._addSortArrow('author')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'author')}>User Name</span>
              </TableHeaderColumn>
              <TableHeaderColumn key={'title'}>
                {this._addSortArrow('title')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'title')}>Post title</span>
              </TableHeaderColumn>
              <TableHeaderColumn key={'views'}>
                {this._addSortArrow('views')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'views')}>Views</span>
              </TableHeaderColumn>
              <TableHeaderColumn key={'likes'}>
                {this._addSortArrow('likes')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'likes')}>Likes</span>
              </TableHeaderColumn>
              <TableHeaderColumn key={'created'}>
                {this._addSortArrow('created')}
                <span className="caption" style={{ cursor: 'pointer' }} onClick={this._setOrderBy.bind(this, 'created')}>Created at</span>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            deselectOnClickaway={false}
          >
            {
              this.props.data.map((row) => {
                return (
                  <TableRow className={(row.author === this.props.loggedIn) ? 'highlighted' : ''} key={Uuid()}>
                    <TableRowColumn>{row.id}</TableRowColumn>
                    <TableRowColumn>{row.author}</TableRowColumn>
                    <TableRowColumn>{row.title}</TableRowColumn>
                    <TableRowColumn>{row.views}</TableRowColumn>
                    <TableRowColumn>{row.likes}</TableRowColumn>
                    <TableRowColumn>{row.created}</TableRowColumn>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>
      </div>
    );
  }
}
