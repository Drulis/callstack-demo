import DataTable from './dataTable';
import DataTablePagination from './dataTablePagination';

export {
  DataTable,
  DataTablePagination,
};
