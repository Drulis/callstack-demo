import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Snackbar } from 'material-ui';
import { showSnackbar } from '../../redux/actions/notificationActions';

// tried to write this as stateless function but eslint has troubles finding propTypes
// will look at this later
class Notifications extends Component {
  static get propTypes() {
    return {
      snackbarOpen: PropTypes.bool,
      message: PropTypes.string,
    };
  }

  render() {
    return (
      <Snackbar
        className="snackbarRoot"
        open={this.props.snackbarOpen}
        message={this.props.message}
        autoHideDuration={4000}
        onRequestClose={showSnackbar.bind(this, false)}
      />
    );
  }
}

export default connect(
  (state) => {
    return {
      snackbarOpen: state.notification.snackbarOpen,
      message: state.notification.message,
    };
  },
)(Notifications);
