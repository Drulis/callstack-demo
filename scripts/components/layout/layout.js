import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Content from './content';
import Notifications from './notifications';
import AppToolbar from './appToolbar';

export default class Layout extends React.Component {
  constructor() {
    super();
    this.displayName = 'Main';
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div className="mainAppContainer">
          <AppToolbar />
          <Content {...this.props} />
          <Notifications />
        </div>
      </MuiThemeProvider>
    );
  }
}
