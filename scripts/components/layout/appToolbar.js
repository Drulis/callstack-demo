import React from 'react';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';

const styles = {
  toolbar: {
    backgroundColor: '#00BCD4',
    height: '80px',
  },
  logoWrapper: {
    marginLeft: '24px',
    color: 'white',
    fontSize: 30,
  },
};

const AppToolbar = () => {
  return (
    <Toolbar style={styles.toolbar}>
      <ToolbarGroup firstChild>
        <div style={styles.logoWrapper}>
          React + Redux + Redux-Thunk
        </div>
      </ToolbarGroup>
    </Toolbar>
  );
};

export default AppToolbar;
