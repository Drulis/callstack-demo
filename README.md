
### Setup & Launch

**Step 1** Install dependencies:

```shell
$ npm install
```

**Step 2**. Launch mock api in separate terminal window: 

```shell
$ npm run mock-api
```

**Step 2**. Launch app: 

```shell
$ npm start 
```
or
```shell
$ node server.js
```

---
Made by Mateusz Drulis
