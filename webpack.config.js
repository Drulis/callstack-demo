const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
  devtool: 'eval-source-map',
  cache: true,
  entry: {
    index: [
      'webpack-hot-middleware/client?path=/__webpack_hmr',
      './scripts/index.js'
    ]
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'main.js',
    publicPath: '/build/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      "process.env": {        
        'NODE_ENV': JSON.stringify('development'),
        'BABEL_ENV': JSON.stringify('dev')
      }
    }),
  ],
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader?importLoader=1&sourceMap',
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.scss$/,
        loaders: ["style", "css", "sass"],
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff",
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/octet-stream",
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=image/svg+xml",
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.(jpg|jpeg|gif|png|ico)$/,
        loader: 'file-loader?limit=100000',
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets')
      }, {
        test: /\.js|\.js?$/,
        loaders: [ 'react-hot', 'babel-loader'],
        exclude: /node_modules/,
        include: path.join(__dirname, 'scripts')
      }, {
        test: /\.json$/,
        loader: "json-loader",
        exclude: /node_modules/,
        include: path.join(__dirname, 'scripts')
      }
    ]
  },
  postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ]
}
